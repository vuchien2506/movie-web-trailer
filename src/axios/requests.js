const API_KEY = "5b5e927bd53f6849fefb1744a7e78447";

const request = {
    fetchTrending: `trending/all/day?api_key=${API_KEY}&language=en-US`,
    fetchFavorite: `trending/movie/week?api_key=${API_KEY}`,
    fetchTopRated: `movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`,
    fetchUpComing: `movie/upcoming?api_key=${API_KEY}&language=en-US&page=1`,
};
export default request;
