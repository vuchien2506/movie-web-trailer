import axios from "axios";

const fetchAPI = axios.create({
    baseURL: "https://api.themoviedb.org/3/",
});

export default fetchAPI;
