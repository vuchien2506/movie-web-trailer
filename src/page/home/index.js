import React from "react";
import request from "../../axios/requests";
import RowWrap from "../../components/row/index";
import Banner from "../../components/banner/index.js";
import NavbarTop from "../../components/navbar/index.js";
import JumboCompound from "../../compounds/JumboCompound";
import FooterCompound from "../../compounds/FooterCompound";
import Seperator from "../../components/Seperator/Seperator";
function Home() {
    return (
        <div>
            <NavbarTop />
            <Banner />
            <RowWrap
                title="Top Trending"
                fetchUrl={request.fetchTrending}
                isLargeRow={true}
            />
            <RowWrap title="Up Coming" fetchUrl={request.fetchUpComing} />
            <RowWrap title="Top Rated" fetchUrl={request.fetchTopRated} />
            <RowWrap title="Favorite Movie" fetchUrl={request.fetchFavorite} />
            <Seperator />
            <JumboCompound />
            <FooterCompound />
        </div>
    );
}
export default Home;
