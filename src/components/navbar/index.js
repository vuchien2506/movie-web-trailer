import React, { useEffect, useState } from "react";
import { Container } from "reactstrap";
import "./style.css";
function NavbarTop() {
    const [show, handleShow] = useState(false);
    useEffect(() => {
        const srollDown = window.addEventListener("scroll", () => {
            if (window.scrollY > 80) {
                handleShow(true);
            } else handleShow(false);
        });
        return srollDown;
    }, []);
    return (
        <div className={`navtop ${show && "nav__black"}`}>
            <Container className={`nav__style`}>
                <img
                    className="nav__logo"
                    alt="Netflix Logo"
                    src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg"
                />

                <img
                    className="nav__user"
                    alt="Netflix User"
                    src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                />
            </Container>
        </div>
    );
}

export default NavbarTop;
