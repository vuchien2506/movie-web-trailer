import React, { useState, useEffect } from "react";
import Youtube from "react-youtube";
import axios from "../../axios/index";
import movieTrailer from "movie-trailer";
import "./style.css";
// Read more https://developers.themoviedb.org/3/getting-started/images with image;
const base_URL = "https://image.tmdb.org/t/p/original/";

function RowWrap({ title, fetchUrl, isLargeRow }) {
    const [movie, setMovie] = useState([]);
    useEffect(
        () => {
            async function fetchData() {
                const data = await axios.get(fetchUrl);
                setMovie(data.data.results);
                return data;
            }
            fetchData();
        },
        // phần tử cuối của url thay đổi => useEffect sẽ load lại.
        [fetchUrl]
    );

    const handleClose = () => {
        if (trailerURL) {
            setTrailerURL("");
        }
    };
    const opts = {
        height: "390",
        width: "100%",
        playerVars: {
            // https://developers.google.com/youtube/player_parameters
            autoplay: 1,
        },
    };
    const [trailerURL, setTrailerURL] = useState("");
    const handleClick = (movie) => {
        if (trailerURL) {
            setTrailerURL("");
        } else {
            // movieTrailer can get name to search on Youtube trailer movie.
            movieTrailer(movie?.name || movie?.title || "")
                // then get url of movie
                .then((url) => {
                    const urlParams = new URLSearchParams(new URL(url).search);
                    setTrailerURL(urlParams.get("v"));
                })
                .catch((error) => console.error(error));
        }
    };
    console.log();
    return (
        <div className="row__wrap">
            <h2 className="row__title">{title}</h2>
            <div className="poster__wrap">
                {movie.map((movie) => (
                    <img
                        className={`poster__banner ${
                            isLargeRow && "poster__LargeRow"
                        }`}
                        key={movie.id}
                        onClick={() => handleClick(movie)}
                        src={`${base_URL}${
                            isLargeRow ? movie.poster_path : movie.backdrop_path
                        }`}
                        alt={movie.original_title}
                    />
                ))}
            </div>
            {trailerURL && (
                <div
                    className="player__overlay"
                    onClick={() => {
                        handleClose();
                    }}
                >
                    <Youtube opts={opts} videoId={trailerURL} />
                </div>
            )}
        </div>
    );
}
export default RowWrap;
