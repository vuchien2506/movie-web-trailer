import React, { useEffect, useState } from "react";
import Youtube from "react-youtube";
import { Container } from "reactstrap";
import movieTrailer from "movie-trailer";
import axios from "../../axios";
import request from "../../axios/requests";
import "./style.css";
const base_URL = "https://image.tmdb.org/t/p/original";
function Banner() {
    const [movie, setMovie] = useState([]);
    useEffect(() => {
        async function getURL() {
            const getMovie = await axios.get(request.fetchUpComing);
            setMovie(
                getMovie.data.results[
                    Math.floor(Math.random() * getMovie.data.results.length)
                ]
            );

            return getMovie;
        }

        getURL();
    }, []);
    const opts = {
        height: "490",
        width: "100%",
        playerVars: {
            autoplay: 1,
        },
    };
    const [trailerURL, setTrailerURL] = useState("");
    const handleClick = (movie) => {
        if (trailerURL) {
            setTrailerURL("");
        } else {
            // movieTrailer can get name to search on Youtube trailer movie.
            movieTrailer(movie?.name || movie?.title || "")
                // then get url of movie
                .then((url) => {
                    const urlParams = new URLSearchParams(new URL(url).search);
                    setTrailerURL(urlParams.get("v"));
                })
                .catch((error) => console.error(error));
        }
    };

    const handleClose = () => {
        if (trailerURL) {
            setTrailerURL("");
        }
    };
    function truncate(str, n) {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str;
    }
    return (
        <header>
            <div
                className="posters__banner"
                style={{
                    backgroundSize: "cover",
                    backgroundPosition: "center center",
                    backgroundImage: `url("${base_URL}${movie?.backdrop_path}")`,
                }}
            >
                <Container className="banner__contents">
                    <h1 className="name__movie">
                        {movie?.title || movie?.name || movie?.original_name}
                    </h1>
                    <div className="banner__button">
                        <button
                            className="click__button"
                            onClick={() => handleClick(movie)}
                        >
                            Play
                        </button>
                        <button className="click__button">Favorite</button>
                    </div>
                    <h5 className="poster__contents">
                        {truncate(movie?.overview, 200)}
                    </h5>
                </Container>

                {trailerURL && (
                    <div
                        className="player__overlay"
                        onClick={() => {
                            handleClose();
                        }}
                    >
                        <Youtube opts={opts} videoId={trailerURL} />
                    </div>
                )}
                <div className="faded__bottom" />
            </div>
        </header>
    );
}

export default Banner;
